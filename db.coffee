IDBStore = require 'idb-wrapper'
parallel = require 'array-parallel'
Semaphore = require 'semaphore.js/semaphore.js'
sem = new Semaphore 1

connectionCache = {}

class CollectionStore
  connectionCount: 0
  storeReady: false
  rangeStore: null
  store: null

  constructor: (name, cb)->
    sem.acquire (release)=>
      @onStoreReady = cb
      if connectionCache["range" + name]
        @rangeStore = connectionCache["range" + name]
      #or create a new one
      else
        @rangeStore = new IDBStore
          dbVersion: 6
          storeName: "range" + name
          indexes:[
            {name: 'begin.date'},
            {name: 'end.date'}
          ]
          onStoreReady: =>
            #store the connection
            connectionCache["range" + name] = @rangeStore
            @_checkIfReady(release)

      #retrieve the store
      if connectionCache[name]
        @store = connectionCache[name]
      #or create a new one
      else
        @store = new IDBStore
          dbVersion: 4
          storeName: name
          autoIncrement: false
          indexes:[
            name: 'rangeId, start', keyPath: ['rangeId','start']
          ]
          onStoreReady: =>
            #store the connection
            connectionCache[name] = @store
            #add models to the store when they get added to the collection
            #@on 'add', (model)->
            #  @store.put(model.toJSON())

            @_checkIfReady(release)
      #store will be ready if in connectionCache
      @_checkIfReady(release)

  ###
  internal callback
  ###
  _checkIfReady: (release)->
    #run the backedup fetchs
    if Object.keys(connectionCache).length  is 2 and not @storeReady
      release()
      #emit on db ready event
      @storeReady = true
      if @onStoreReady
        @onStoreReady(@)

  ###
  find range that the needle fits in between
  @param needle {string}
  @param direction {up|down} 
  @param cb {function} callback is given the range found is any and boolean saying if there is more objects beyond the range
  ###
  findRange: (needle, cb)->
    @rangeStore.getAll (ranges)->
      foundRange = false
      nearstOutlier = false
      #search to find a range that search_date fall in between
      ranges.every (range)->
        #the query lies outside all of the range
        if (range.end.date <= needle and not range.end.more) or (range.begin.date >= needle and not range.begin.more)
          nearstOutlier = range
          return false
       
        if range.end.date >= needle >= range.begin.date
          #found the range
          foundRange = range
          return false
        #no range found and there could be more events
        return true

      cb foundRange, nearstOutlier

  ###
  find objects for a given range
  @method findObjectsInRange
  @param id {Interger} the id of the range
  @param direction {up|down}
  @param onItem {function} runs on every object found
  @param onEnd {function} callback
  ###
  findObjectsInRange: (id, needle, direction, offset, limit,  onEnd)->
    if direction is 'up'
      order = 'ASC'
      keyRange = @store.makeKeyRange
        upper: [id, '9999-10-21T23:08:15.636Z']
        lower: [id, needle]
    else
      order = 'DESC'
      keyRange = @store.makeKeyRange
        upper: [id, needle]
        lower: [id, '0000-01-01T00:00:00.000Z']

    #find events in the store that match the current range
    objects = []
    count = 0
    endCount = offset + limit
    more = false
    onItem = (item)->
      count++
      if endCount >= count > offset
        objects.push item
      else if count > endCount
        more = true

    @store.iterate onItem,
      keyRange: keyRange
      index: 'rangeId, start'
      order: order
      onEnd: ()->
        onEnd(objects, more)

  ###
  find a range for a given needle and the events associated with that range
  @param needle {String}
  @param direction {up|down}
  @param onEnd {function} callback
  ###
  findRangeAndObjects: (needle, direction, cb)->
    @findRange needle, (foundRange, outlier)=>
      objs = []
      range = foundRange or outlier
      if range
        onItem = (item)->
          console.log('you need to implent limits yet!!')
          objs.push item
        @findObjectsInRange range.id, needle, direction, onItem, ()->
          cb foundRange, outlier, objs
      else
        cb foundRange, outlier, objs

  ###
  finds overlapping ranges and deletes them
  ###
  findOverLappingRanges: (range, direction, cb)->
    begin = range.begin.date
    end = range.end.date
    rangeIDs = []
    foundEdge = null

    onEnd = ()->
      cb foundEdge

    onRange = (range, cursor, transaction)->
      if direction is 'up'
        if range.begin.date < end
          if range.end.date > end
            foundEdge = range
          cursor.delete()
      else
        if range.end.date > begin
          if range.begin.date < begin
            foundEdge = range
          cursor.delete()

    if direction is 'up'
      index = 'end.date'
      keyRange = @store.makeKeyRange
        lower: begin
        excludeLower: true
    else
      index = 'begin.date'
      keyRange = @store.makeKeyRange
        upper: end
        excludeUpper: true

    @rangeStore.iterate onRange,
      keyRange: keyRange
      onEnd: onEnd
      index: index
      writeAccess: true

  ###
  inserts a range by merging overlapping ranges and expanding edge ranges
  @param range {Object}
  @param direction {up|down} the direction the inserted range was expanding
  @param cb {function} callback
  ###
  insertRange: (range, direction, cb)->
    @findOverLappingRanges range, direction, (foundEdge) =>
      if direction is "up"
        if foundEdge
          range.end = foundEdge.end
      else
        if foundEdge
          range.begin = foundEdge.begin

      #store the updated range
      @rangeStore.put range, cb

  ###
  Insets a new object into the db and creates or upates the assacated ranges
  @method insertNewObject
  @param {Object} object the object to be inserted
  @param {Boolean} createNewRange
  @param {Function} cb a callback
  ###
  insertNewObject: (object, createNewRange, cb)->
    if typeof(createNewRange) is 'function'
      cb = createNewRange
      createNewRange = true
 
    @findRange object.start, (range, outlier)=>
      if outlier
        object.rangeId = outlier.id
        if outlier.end.date < object.start
          outlier.end.more = true
        else
          outlier.begin.more = true

        #TODO: add sempore take
        @rangeStore.put outlier
     
      #create a new range
      if not range and createNewRange
        range =
          end:
            date: object.start
            more: true
          begin:
            date: object.start
            more: true

        @rangeStore.put range, (id)=>
          object.rangeId = id
          @store.put object, ()->
            #todo semphore aquire and release
            cb()

      else if range
        object.rangeId = range.id
        @store.put object, ()->
          cb()
      else
        cb()

module.exports = CollectionStore
