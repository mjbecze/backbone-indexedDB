###
@module MetaGeo Models
###

Backbone = require 'backbone'
Idb = require './backbone-indexedDB/db.js'
IDBsync = require './backbone-indexedDB/idbSync'
Semaphore = require 'semaphore.js/semaphore.js'
_ = require 'underscore'

#http://kilon.org/blog/2012/02/backbone-calculated-fields/
class Model extends Backbone.Model
  get:(attr) ->
    value = Backbone.Model::get.call(this, attr)
    (if _.isFunction(value) then value.call(this) else value)

  toJSON: ->
    data = {}
    json = Backbone.Model::toJSON.call(this)
    _.each json, ((value, key) ->
      data[key] = @get(key)
    ), this
    data

class IDBModel extends Model
  sem:  new Semaphore(1)
  sync: IDBsync.modelSync
  constructor: (attributes, options)->
    @sem.acquire (release)=>
      name = @tableName or @constructor.name
      new Idb name, (db)=>
        @db = db
        release()
    super

  fetch: (options)->
    success = options.success
    error = options.error
    @sem.acquire (release)=>
      options.success = (data,request)->
        if success
          success.apply @, arguments
        release()
      options.error = ()->
        if error
          error.apply @, arguments
        release()

      super(options)

#keeps track of what events the collection has
RangeCursor = ->
  begin:
    more: true
    loading: false
    offset: 0
  end:
    loading: false
    offset: 0
    more: true

class IDBCollection extends Backbone.Collection

  sem:  new Semaphore(1)
  origin: false
  range:
    begin:{}
    end:{}
  #store fetchs made before the db init
  fetchCache: []
  sync: IDBsync.collectionSync

  ###
  Overload the contructor to set up indexedDb
  ###
  constructor: ()->
    #range Cursor keeps track of where this collection has fetched from
    @rangeCursor = new RangeCursor
    @sem.acquire (release)=>
      name = @tableName or @constructor.name
      new Idb name, (db)=>
        @db = db
        release()
    super()

  ffetch: ()->
    @dirFetch("forward")

  rfetch: ()->
    @dirFetch("reverse")

  dirFetch: (direction)->
    if direction is "forward"
      dir = @rangeCursor.end
    else
      dir = @rangeCursor.begin
    #if the api or indexedDb has more events the get more
    if dir.more
      @sem.acquire (release)=>
        console.log('fetching ' + direction)
        data =
          offset: dir.offset
          limit: @apiSettings.limit

        data[@apiSettings[direction]] = @origin

        @fetch(
          remove: false
          success: (events, request)=>
            dir.offset += events.length
            release()
          data: data
        )

  update:(cb)->
    @rangeCursor.end.more = true
    @rangeCursor.begin.more = true
    data = {}
    data[@apiSettings['forward']] = @range.begin.date
    data[@apiSettings['reverse']] = @range.end.date
    data[@apiSettings['update']] = @range.updated

    @fetch(
      success: (events, request)=>
        if  _.isFunction cb then cb()
      data: data
      remove: false
    )

  setOrigin:(origin)->
    @sem.acquire (release)=>
      console.log('setting origin'+ origin)
      @origin = origin
      @db.findRange origin, (foundRange, outlier)=>
        @range = foundRange or outlier
        if not @range
          @range =
            begin:
              more: true
              date: origin
            end:
              more: true
              date: origin
          @db.rangeStore.put @range, (id)=>
            @range.id = id
            release()
        else
          #update the range with the lastest events
          @update(release)

module.exports =
  Model: Model
  IDBModel: IDBModel
  IDBCollection: IDBCollection
