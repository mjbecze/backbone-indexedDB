###
indexdDB sync, provides a multi-layer persistence adapter for backbone
###
Backbone = require 'backbone'

###
replaces Backbone.sync for models; syncs the objects in indexeDb in a table
named from the model name and then syncs to the server using Backbone.sync,
works for update, delete, fetch and create
@method modelSync
###
modelSync =  (method, model, options)->
  #create the model in indexedDB and on the server
  if method is 'create'
    success = options.success
    options.success = (data, request)=>
      args = arguments
      model = @parse data
      #save the model if a range is found that it fits in
      @db.insertNewObject model, false, ()->
        success.apply @, args

    if options.backboneSync isnt false
      Backbone.sync method, model, options
    else
      options.success(model)

  else if method is 'delete'
    success = options.success
    options.success = (data, request)=>
      @db.store.remove model.id, ()->
        success data, request
    Backbone.sync method, model, options

  else if method is 'read'
    #save the orginal success callback
    success = options.success
    #check to see if the model is in indexedDB
    @db.store.get model.id, (object) =>
      #if the model was found call the success callback
      if object?
        success object
      else
        #if the model was not found 
        options.success = (data,request,roptions)=>
          #save the fetched object in indexedDB; create a new range if nessicary
          @db.insertNewObject data, ()->
            #run the oringal success callback
            success data, request, roptions
        Backbone.sync method, model, options

  else if method is 'update'
    #save the orginal success callback
    success = options.success
    options.success = (data, request, roptions)=>
      #save the fetched object in indexedDB
      @db.insertNewObject data, ()->
        #run the oringal success callback
        success data, request, roptions

    Backbone.sync method, model, options
  else
    return Backbone.sync method, model, options

###
replaces Backbone.sync for collection; saves the objects in fetched by Backbone.sync in a table named from the collection's model and then saves the range of objects it fetched in a range table so that it know wether to look on the server or indexedDB for more events
@method collectionSync
###
collectionSync = (method, model, options)->
  foundItems = []

  if options.data['updated[gt]']
    return collectionUpdateSync.apply @, arguments
  else if options.data['start[gt]']
    search_date = options.data['start[gt]']
    action = 'up'
    rangeCursor = @rangeCursor.end
  else if options.data['start[lt]']
    search_date = options.data['start[lt]']
    action = 'down'
    rangeCursor = @rangeCursor.begin
  else
    Backbone.sync.apply @, arguments

  #save the origanl success cb
  _success = options.success
  #and wrap it so that the original ajax sync will work that same.
  success = (objs, request)=>
    #relace parse temparaly so that when Backbone.sync call success it will be able to get the objects from it
    _parse = @parse
    @parse = ()->
      objs
    _success(objs, request)
    @parse = _parse

  options.success = (data, request)=>
    #run the orignal success function
    objects = @parse data

    #set the rangeCursor so that the collection know if there is more objects
    rangeCursor.more = @parseMore data
    #set the time that this range was updated
    @range.updated = data.serverTime
    if action is 'up'
      @range.end =
        #the last item in the array is now the end of the range
        date: if data.items.length > 0 then objects[data.items.length - 1].start else @range.end.date
        more: @parseMore data
    else if action is 'down'
      @range.begin =
        #the first item in the array is now the begining of the range
        date: if data.items.length > 0 then objects[0].start else @range.begin.date
        more: @parseMore data

    #insert the range
    @db.insertRange @range, action, (id)=>
      if objects.length > 0
        objects.forEach (obj)->
          obj.rangeId = id
        #store the objects with the range id
        @db.store.putBatch objects, ()=>
          success objects.concat(foundItems), request
      else
        success objects.concat(foundItems), request
  
  #find objeccts in the current range and if there are possible more fetch from the server
  @db.findObjectsInRange @range.id, @origin, action, options.data.offset, options.data.limit, (objs, more)=>
    foundItems = objs
    #is there possible more events on the server?
    if not more and ((action is 'up' and @range.end.more) or (action is 'down' and @range.begin.more))
      options.data.offset += objs.length
      #use the orginal fetch
      return Backbone.sync(method, model, options)
    else
      rangeCursor.more = more
      #todo: check this
      success foundItems

###
used to look for objects that have been updated since the sync has occured
@method collectionUpdateSync
###
collectionUpdateSync = (method, model, options)->
  #save the origanl success cb
  _success = options.success
  #and wrap it so that the original ajax sync will work that same.
  success = (objs, request)=>
    #relace parse temparaly so that when Backbone.sync call success it will be able to get the objects from it
    _parse = @parse
    @parse = ()->
      objs
    _success(objs, request)
    @parse = _parse

  options.success = (data, request)=>
    objects = @parse data
    @range.updated = data.serverTime
    @range.end.more = true
    @range.begin.more = true
    @rangeCursor.end.more = true
    @rangeCursor.begin.more = true
    @db.rangeStore.put @range, ()=>
      if objects.length > 0
        @db.store.putBatch objects, ()->
          success objects, request
      else
        success [], request

  Backbone.sync(method, model, options)

module.exports =
  modelSync: modelSync
  collectionSync: collectionSync
